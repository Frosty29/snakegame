// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "Interactable.h"
#include "Bonus.h"
#include "Antibonus.h"
#include "Food.h"
#include "SnakeElementBase.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	SpawnFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	
	//������ 3 ��� ������� ���:
	FoodTime += DeltaTime;
	if (FoodTime >= 3.0f)
	{
		SpawnFood();
		FoodTime = 0;
	}
	//
	//������ 10 ��� ������� ����� ��� ���������:
	int j = FMath::RandRange(0, 1);
	
	BonusTime += DeltaTime;
	if (BonusTime>=10.0f && j==0)
	{
		SpawnBonus();
		BonusTime = 0;
		BonusNumber++;
	}
	else if (BonusTime >= 10.0f && j == 1)
	{
		SpawnAntibonus();
		BonusTime = 0;
		AntibonusNumber++;
	}
	//
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		//������ ����� ������� � ������:
		NewSnakeElem->SetActorHiddenInGame(true);	
		//

		int32 ElemIndex=SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) 
		{
			NewSnakeElem->SetFirstElementType();
		}
		
	}
	
}


void ASnakeBase::Move()
{	
	FVector MovementVector(ForceInitToZero);
		
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		//���������� ���������� ��������:
		CurrentElement->SetActorHiddenInGame(false);
		PrevElement->SetActorHiddenInGame(false);
		//
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

//�������� ��� � ��������� �����, ������������ ���������:
void ASnakeBase::SpawnFood()
{
	if (FoodNumber >= 3)
	{
		return;
	}
	else 
	{
		int x = FMath::RandRange(-9, 9);
		int y = FMath::RandRange(-9, 9);
		FVector Location(x * 50, y * 50, -30);
		FTransform Transform(Location);

		AFood* food = GetWorld()->SpawnActor<AFood>(FoodElementClass, Transform);
		FoodNumber++;
	}
}
//�������� ������ � ��������� �����, ������������ ���������:
void ASnakeBase::SpawnBonus()
{
	if (BonusNumber >= 1)
	{
		return;
	}
	else
	{
		int x = FMath::RandRange(-8, 8);
		int y = FMath::RandRange(-8, 8);
		FVector Location(x * 50, y * 50, -30);
		FTransform Transform(Location);

		ABonus* bonus = GetWorld()->SpawnActor<ABonus>(BonusElementClass, Transform);
	}
}
//�������� ���������� � ��������� �����, ������������ ���������:
void ASnakeBase::SpawnAntibonus()
{
	if (AntibonusNumber >= 1)
	{
		return;
	}
	else
	{
		int x = FMath::RandRange(-8, 8);
		int y = FMath::RandRange(-8, 8);
		FVector Location(x * 50, y * 50, -30);
		FTransform Transform(Location);

		AAntibonus* antibonus = GetWorld()->SpawnActor<AAntibonus>(AntibonusElementClass, Transform);
	}
}

//��������� (������������ �������� ������)
void ASnakeBase::IncreaseSpeed()
{
	MovementSpeed = 0.1;
	SetActorTickInterval(MovementSpeed);
	AntibonusNumber--;
}
//�����(���������� 1 ������� ������ � ����������� ��������):
void ASnakeBase::GetBonus()
{
	MovementSpeed = 0.2;
	SetActorTickInterval(MovementSpeed);
	SnakeElements.Pop()->Destroy();
	BonusNumber--;
}
//����� ���:
void ASnakeBase::FoodMinus()
{
	FoodNumber--;
}


