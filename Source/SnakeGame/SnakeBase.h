// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
//
class AFood;
class ABonus;
class AAntibonus;
//
UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	
	

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*>  SnakeElements;
	
	UPROPERTY()
		EMovementDirection LastMoveDirection;

	//
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodElementClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusElementClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AAntibonus> AntibonusElementClass;
	//
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum=1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	
	//
	UPROPERTY(EditDefaultsOnly)
	float FoodTime = 0.0f;
	float BonusTime = 0.0f;

	int FoodNumber = 0;
	int BonusNumber = 0;
	int AntibonusNumber = 0;

	UFUNCTION()
	void SpawnFood();
	void SpawnBonus();
	void SpawnAntibonus();
	void IncreaseSpeed();
	void GetBonus();
	void FoodMinus();
	
	//
};
